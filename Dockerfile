FROM openjdk:8-jdk-alpine
LABEL maintainer="krishna.silalahi@gmail.com"
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/account-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} account.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/account.jar"]