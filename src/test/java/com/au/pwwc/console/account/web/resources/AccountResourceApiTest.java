package com.au.pwwc.console.account.web.resources;

import com.au.pwwc.console.account.AccountApplication;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@ActiveProfiles("apitest")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccountApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountResourceApiTest {
    private Logger log = LoggerFactory.getLogger(AccountResourceApiTest.class);

    Map<String,String> user;
    @LocalServerPort
    private int port;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost/api";
        user = new HashMap<>();
        user.put("firstName", "Test");
        user.put("surname", "User");
        user.put("email", "test.user@gmail.com");
    }

    @Test
    public void testGetUserReturnsCorrectResponse() {
        log.info("Starts testing get user");
        RestAssured.registerParser("application/json", Parser.JSON);
        given().port(port)
            .when()
            .get("/user/1")
            .then()
            .statusCode(200).contentType(ContentType.JSON)
            .assertThat()
            .body("firstName", equalTo("Krishna"))
            .body("surname", equalTo("Silalahi"))
            .body("email", equalTo("krishna.silalahi@gmail.com"));
    }

    @Test
    public void testGetUserReturnsFailedResponse() {
        log.info("Starts testing get user failed");
        given().port(port)
            .when()
            .get("/user/11")
            .then()
            .statusCode(404);
    }

    @Test
    public void testDeleteUserReturnsCorrectResponse() {
        log.info("Starts testing delete user");
        RestAssured.registerParser("application/json", Parser.JSON);
        given().port(port)
            .when()
            .delete("/user/1")
            .then()
            .statusCode(200).contentType(ContentType.JSON)
            .assertThat()
            .body("firstName", equalTo("Krishna"))
            .body("deleted", equalTo(true));
    }

    @Test
    public void testDeleteUserReturnsFailedResponse() {
        log.info("Starts testing delete user failed");
        given().port(port)
            .when()
            .delete("/user/11")
            .then()
            .statusCode(404);
    }

    @Test
    public void testAddUserReturnsCorrectResponse() {
        log.info("Starts testing add user");
        RestAssured.registerParser("application/json", Parser.JSON);
        given().port(port)
            .contentType(ContentType.JSON)
            .body(user)
            .when()
            .post("/user")
            .then()
            .statusCode(200)
            .assertThat()
            .body("firstName", equalTo("Test"));
    }

    @Test
    public void testAddUserReturnsFailedResponse() {
        log.info("Starts testing add user failed");
        user.put("email", "krishna.silalahi@gmail.com");
        RestAssured.registerParser("application/json", Parser.JSON);
        given().port(port)
            .contentType(ContentType.JSON)
            .body(user)
            .when()
            .post("/user")
            .then()
            .statusCode(400);
    }

    @After
    public void clear() {}
}
