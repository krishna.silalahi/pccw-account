package com.au.pwwc.console.account.services;

import com.au.pwwc.console.account.repositories.UserRepository;
import com.au.pwwc.console.account.repositories.model.User;
import com.au.pwwc.console.account.services.impl.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ActiveProfiles("unittest")
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceUnitTest {
    private User user;
    private com.au.pwwc.console.account.web.types.User postUser;
    private Logger log = LoggerFactory.getLogger(UserServiceUnitTest.class);

    @Mock
    UserRepository<User> userRepositoryMock;

    @InjectMocks
    private UserService userService = new UserService();

    @Before
    public void setup() {
        user = new User();
        user.setId((long) 1);
        user.setFirstName("Test");
        user.setSurname("User");
        user.setEmail("test.user@gmail.com");
        user.setDeleted(false);

        postUser = new com.au.pwwc.console.account.web.types.User();
        postUser.setFirstName("Test");
        postUser.setSurname("User");
        postUser.setEmail("test.user@gmail.com");
    }

    @Test
    public void testGetUser() {
        log.info("Starts testing get user");

        // set the mock user to be returned
        when (userRepositoryMock.getOne(anyLong())).thenReturn(user);
        log.info("USER: " + user.toString());

        com.au.pwwc.console.account.web.types.User returnedUser = userService.getUser(Long.parseLong("1"));
        log.info("RETURNED USER: " + returnedUser.toString());

        Assert.assertEquals("Get user returns incorrect email", "test.user@gmail.com", returnedUser.getEmail());
    }

    @Test
    public void testDeleteUser() {
        log.info("Starts testing delete user");

        // set the mock user to be returned
        when (userRepositoryMock.save(any())).thenReturn(user);
        log.info("USER: " + user.toString());

        postUser.setId("1");
        postUser.setDeleted(true);
        com.au.pwwc.console.account.web.types.User returnedUser = userService.saveUser(postUser);
        log.info("RETURNED USER: " + returnedUser.toString());

        Assert.assertEquals("Update user returns incorrect email", "test.user@gmail.com", returnedUser.getEmail());
        user.setDeleted(true);
        verify(userRepositoryMock).save(ArgumentMatchers.eq(user));
    }

    @Test
    public void testSaveUser() {
        log.info("Starts testing save user");

        // set the mock user to be returned
        when (userRepositoryMock.save(any())).thenReturn(user);
        log.info("USER: " + user.toString());

        com.au.pwwc.console.account.web.types.User returnedUser = userService.saveUser(postUser);
        log.info("RETURNED USER: " + returnedUser.toString());

        Assert.assertEquals("Save user returns incorrect email", "test.user@gmail.com", returnedUser.getEmail());
        user.setId(null);
        user.setDeleted(null);
        verify(userRepositoryMock).save(ArgumentMatchers.eq(user));
    }

    @Test
    public void testUpdateUser() {
        log.info("Starts testing update user");

        // set the mock user to be returned
        when (userRepositoryMock.save(any())).thenReturn(user);
        log.info("USER: " + user.toString());

        postUser.setId("1");
        com.au.pwwc.console.account.web.types.User returnedUser = userService.saveUser(postUser);
        log.info("RETURNED USER: " + returnedUser.toString());

        Assert.assertEquals("Update user returns incorrect email", "test.user@gmail.com", returnedUser.getEmail());
        user.setDeleted(null);
        verify(userRepositoryMock).save(ArgumentMatchers.eq(user));
    }
}
