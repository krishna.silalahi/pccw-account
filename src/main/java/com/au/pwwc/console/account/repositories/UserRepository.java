package com.au.pwwc.console.account.repositories;

import com.au.pwwc.console.account.repositories.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository<U> extends CrudRepository<User, Long> {
    List<User> findAll();
    User getOne(Long id);
    User save(User entity);
    boolean existsById(Long id);
    void delete(User entity);
    void deleteById(Long id);
}
