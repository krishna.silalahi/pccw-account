package com.au.pwwc.console.account.repositories.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "users")
@ApiModel(description = "User record")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "User ID")
    private Long id;

    @NotNull
    @Column(name = "firstName")
    @ApiModelProperty(notes = "User First Name", required = true)
    private String firstName;

    @NotNull
    @Column(name = "surname")
    @ApiModelProperty(notes = "User Surname", required = true)
    private String surname;

    @Email
    @NotNull
    @Column(name = "email", unique = true)
    @ApiModelProperty(notes = "User Email", required = true)
    private String email;

    @Column(name = "deleted", columnDefinition="tinyint(1) default 0")
    @ApiModelProperty(notes = "User active status, default active")
    private Boolean deleted;
}
