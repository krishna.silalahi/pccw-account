package com.au.pwwc.console.account.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class Emailer {
    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private int port;

    @Value("${spring.mail.username}")
    private String username;

    @Value("${spring.mail.password}")
    private String password;

    public void sendSuccessfulRegistrationEmail(String emailTo, String firstName) {
        // Create a mail sender
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        // Create an email instance
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("no-reply@pccw-account.com");
        mailMessage.setTo(emailTo);
        mailMessage.setSubject("Successful registration to PCCW Account");
        mailMessage.setText("Hi " + firstName + ",\nYour registration is successful");

        // Send mail
        mailSender.send(mailMessage);
    }
}
