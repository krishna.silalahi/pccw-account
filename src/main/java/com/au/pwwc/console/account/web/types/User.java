package com.au.pwwc.console.account.web.types;

import lombok.Data;

@Data
public class User {
    String id;
    String firstName;
    String surname;
    String email;
    Boolean deleted;
}
