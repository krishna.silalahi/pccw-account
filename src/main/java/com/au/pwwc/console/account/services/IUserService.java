package com.au.pwwc.console.account.services;

import com.au.pwwc.console.account.web.types.User;

public interface IUserService {
    User getUser(Long id);
    User saveUser(User user);
    boolean updateUser(User user);
    void deleteUser(Long id);
}
