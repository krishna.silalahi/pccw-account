package com.au.pwwc.console.account.web.controllers;

import com.au.pwwc.console.account.exceptions.InternalServerErrorException;
import com.au.pwwc.console.account.exceptions.UserNotFoundException;
import com.au.pwwc.console.account.services.IUserService;
import com.au.pwwc.console.account.utils.Emailer;
import com.au.pwwc.console.account.web.types.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
public class AccountController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    IUserService userService;

    @Autowired
    Emailer emailer;

    /**
     * Get a user
     * @param id User Id
     * @return response string
     */
    @GetMapping("/user/{id}")
    @ApiOperation(value = "View a single User")
    public @ResponseBody
    User getUser(
            @ApiParam(value = "Employee Id to update employee object", required = true)
            @PathVariable("id") Long id) {
        try {
            return userService.getUser(id);
        } catch (Exception e) {
            if(e.getCause().getCause() instanceof EntityNotFoundException) {
                logger.error("Failed fetching user: " + e.getCause().getCause().getMessage());
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find user with specified id", e);
            }

            logger.error("Failed fetching user: " + e.getMessage());
            throw new InternalServerErrorException("Failed fetching user");
        }
    }

    /**
     * Delete existing user
     * @param id User id
     */
    @DeleteMapping("/user/{id}")
    @ApiOperation(value = "Delete a single User")
    public @ResponseBody User deleteUser(
            @ApiParam(value = "Employee Id to update employee object", required = true)
            @PathVariable("id") Long id) {
        try {
            User user = userService.getUser(id);
            user.setDeleted(true);
            return userService.saveUser(user);
        } catch (Exception e) {
            if(e.getCause().getCause() instanceof EntityNotFoundException) {
                logger.error("Failed deleting user: " + e.getCause().getCause().getMessage());
                throw new UserNotFoundException("Unable to delete user with specified id");
            }

            logger.error("Failed deleting user: " + e.getCause().getMessage());
            throw new InternalServerErrorException("Failed deleting user");
        }
    }

    /**
     * Create new user
     * @param newUser The User object post
     * @return User response string
     */
    @PostMapping("/user")
    @ApiOperation(value = "Create a User")
    public @ResponseBody User addUser(
            @ApiParam(value = "New user Object", required = true)
            @Valid @RequestBody User newUser) {
        try {
            User user = userService.saveUser(newUser);
            emailer.sendSuccessfulRegistrationEmail(user.getEmail(), user.getFirstName());
            return user;
        } catch (MailException e) {
            logger.error("Failed sending email to new user: " + e.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed sending email", e);
        } catch (Exception e) {
            if(e.getCause() instanceof ConstraintViolationException) {
                logger.error("Failed adding new user: " + e.getCause().getMessage());
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with same email already exists", e);
            }

            logger.error("Failed adding new user: " + e.getMessage());
            throw new InternalServerErrorException("Failed adding new user");
        }
    }

    /**
     * Update existing user
     * @param user User Json
     * @return response String
     */
    @PutMapping("/user")
    @ApiOperation(value = "Update a User")
    public @ResponseBody User updateUser(
            @ApiParam(value = "New user Object", required = true)
            @Valid @RequestBody User user) {
        logger.info("UPDATE USER OBJECT: " + user.toString());
        try {
            return userService.saveUser(user);
        } catch (Exception e) {
            if(e.getCause() instanceof ConstraintViolationException) {
                logger.error("Failed updating user: " + e.getCause().getMessage());
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with same email already exists", e);
            }

            logger.error("Failed updating user: " + e.getMessage());
            throw new InternalServerErrorException("Failed updating user");
        }
    }
}
