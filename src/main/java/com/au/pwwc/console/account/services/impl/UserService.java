package com.au.pwwc.console.account.services.impl;

import com.au.pwwc.console.account.repositories.UserRepository;
import com.au.pwwc.console.account.repositories.model.User;
import com.au.pwwc.console.account.services.IUserService;
import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class UserService implements IUserService {
    @Autowired
    UserRepository<User> userRepository;

    DozerBeanMapper mapper;

    public UserService() {
        this.mapper = new DozerBeanMapper();
    }

    /**
     * Service to get user object
     * @param id User id
     * @return User response
     */
    @Transactional
    public com.au.pwwc.console.account.web.types.User getUser(Long id) {
        User user = userRepository.getOne(id);
        return mapper.map(user, com.au.pwwc.console.account.web.types.User.class);
    }

    /**
     * Service to create new user object
     * @param user User object
     * @return User created
     */
    @Transactional
    public com.au.pwwc.console.account.web.types.User saveUser(com.au.pwwc.console.account.web.types.User user) {
        User newUser = userRepository.save(mapper.map(user, User.class));
        return mapper.map(newUser, com.au.pwwc.console.account.web.types.User.class);
    }

    /**
     * Service to update new user object
     * @param user User object
     * @return User updated
     */
    @Transactional
    public boolean updateUser(com.au.pwwc.console.account.web.types.User user) {
        return userRepository.save(mapper.map(user, User.class)) != null;
    }

    public void deleteUser(Long id) {
        this.userRepository.deleteById(id);
    }
}
